
var exec = require('cordova/exec');
var cordova = require('cordova');

var eventListener = [];

/**
  Called when an 'exec' call succeeds.
*/
var onSuccess = function(event) {
  console.log("onSuccess: "+event);
};

/**
  Called when an 'exec' call fails.
*/
var onFailure = function(event, error) {
  console.log("Error while calling "+event+": "+error);
};

var anyware = {

  /**
    Calls the startEngine method.
  */
  start: function(uuid) {
    console.log('start: '+uuid);
    exec(function() {
      onSuccess('start');
    }, function(error) {
      onFailure('start', error);
    }, "Anyware", "startEngine", [uuid]);
  },

  /**
    Calls the stopEngine method.
  */
  stop: function() {
    exec(function() {
      onSuccess('stop');
    }, function(error) {
      onFailure('stop', error);
    }, "Anyware", "stopEngine", []);
  },

  /**
    Calls the getNearestZone method.
    If it succeeds, return the zone. If not, return the error.
  */
  getNearestZone: function(cb) {
    exec(function(zone) {
      onSuccess('getNearestZone');
      cb(null, zone);
    }, function(error) {
      onFailure('getNearestZone', error);
      cb(error);
    }, "Anyware", "getNearestZone", []);
  },

  /**
    Calls the bindEngineWithUserId method.
  */
  bindCustomId: function(key, customId) {
    exec(function() {
      onSuccess('bindCustomId');
    }, function(error) {
      onFailure('bindCustomId', error);
    }, "Anyware", "bindEngineWithUserId", [key, customId]);
  },

  /**
    Calls the getDeviceId method.
    If it succeeds, returns the device id. If not, returns the error.
  */
  getDeviceId: function(cb) {
    exec(function(deviceId) {
      onSuccess('getDeviceId');
      cb(null, deviceId);
    }, function(error) {
      onFailure('getDeviceId', error);
      cb(error);
    }, "Anyware", "getDeviceId", []);
  },

  /**
    Calls the getNotificationsSince method.
    The date parameter must have the following format : "yyyy-MM-dd'T'HH:mm:ss'.'SSSZ" example: "2015-06-25T14:21:00.000-0400"
    If it succeeds, returns the notifications (push and local) that were generated after the provided date. If not, returns the error.
  */
  getNotificationsSince: function(date, cb) {
    exec(function(notifications) {
      onSuccess('getNotificationsSince');
      cb(null, notifications);
    }, function(error) {
      onFailure('getNotificationsSince', error);
      cb(error);
    }, "Anyware", "getNotificationsSince", [date]);
  },

  /**
    Calls the findDevicesFromUserIds method.
    On iOS, will return the devices in the callback.
    On Android, will return devices through the event "device_locations".
  */
  findDevicesFromUserIds: function(key, userIds, cb) {
    exec(function(userLocations) {
      onSuccess('findDevicesFromUserIds');
      cb(null, userLocations);
    }, function(error) {
      onFailure('findDevicesFromUserIds', error);
      cb(error);
    }, "Anyware", "findDevicesFromUserIds", [key, userIds]);
  },

  /**
    Calls the setNotificationsEnabled method.
  */
  setNotificationsEnabled: function(enabled) {
    exec(function() {
      onSuccess('setNotificationsEnabled');
    }, function(error) {
      onFailure('setNotificationsEnabled', error);
    }, "Anyware", "setNotificationsEnabled", [enabled]);
  },

  /**
    Calls the setupNotifications method (Android only).
  */
  setupNotifications: function(options) {
    exec(function() {
      onSuccess('setupNotifications');
    }, function(error) {
      onFailure('setupNotifications', error);
    }, "Anyware", "setupNotifications", [options]);
  },

  /**
    Sets a listener (callback) for a specific event.
  */
  on: function(event, cb) {
    console.log("Registering callback for event: "+event);
    eventListener[event] = cb;
  },

  /**
    Calls the callback registered by the user for a specific event, if it exists.
    Used internaly. Should not be used by the app.
  */
  notify: function(event, data) {
    console.log("notify "+event);
    if(eventListener[event]) {
      console.log("Notifying client of event: "+event);
      eventListener[event](data);
    }
  }
};

module.exports = anyware;