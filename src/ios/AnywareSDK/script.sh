#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    echo "Usage: ./script pdp_meteor_directory"
    exit 1
fi

# Get the last commit hash.
cordova_plugin_directory=`pwd`
git pull
hash=`git log -n 1 --pretty=format:"%H"`

# Replace the hash in meteor project.
cd "$1/.meteor"
sed -E -i '' "s/https:\/\/bitbucket\.org\/novom\/anyware-sdk-cordova\/get\/.*\.tar\.gz/https:\/\/bitbucket\.org\/novom\/anyware-sdk-cordova\/get\/$hash\.tar\.gz/" cordova-plugins

# Run meteor on iOS device.
cd ..
meteor run ios-device &

sleep 120

# Copy the model.
cd $cordova_plugin_directory
cp -R AnywareModel.xcdatamodeld "$1/.meteor/local/cordova-build/platforms/ios/Fête du Lac/Plugins/com.novomnetworks.anyware.cordova.plugin/"
