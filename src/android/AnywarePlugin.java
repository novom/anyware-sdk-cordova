package com.novomnetworks.anyware.cordova;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.Exception;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import novom.anyware.anywaresdk.AWREngine;
import novom.anyware.anywaresdk.AWREngineListener;
import novom.anyware.anywaresdk.AnywareEngine;
import novom.anyware.anywaresdk.dbobjects.AWRGPSDetection;
import novom.anyware.anywaresdk.dbobjects.AWRZoneEvent;
import novom.anyware.anywaresdk.dbobjects.AWRZone;

public class AnywarePlugin extends CordovaPlugin implements AWREngineListener {
    public static final String TAG = "AnywarePlugin";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSSZ");

    private static AnywarePlugin instance;
    private static ArrayList<Intent> notificationQueue = new ArrayList<Intent>();

    private CordovaWebView webView;
    private AnywareEngine engine;
    private ArrayList<String> actions;

    /**
     * Constructor.
     */
    public AnywarePlugin() {
        Log.v(TAG, "Constructing AnywarePlugin");
        instance = this;
        actions = new ArrayList<String>();
        actions.add("startEngine");
        actions.add("stopEngine");
        actions.add("bindEngineWithUserId");
        actions.add("getDeviceId");
        actions.add("getNearestZone");
        actions.add("getNotificationsSince");
        actions.add("findDevicesFromUserIds");
        actions.add("setNotificationsEnabled");
        actions.add("setupNotifications");
        actions.add("on");
    }

    public static AnywarePlugin getInstance() {
        return instance;
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        Log.v(TAG, "Initializing AnywarePlugin");
        super.initialize(cordova, webView);
        this.webView = webView;
        engine = getEngine();
    }

    public AnywareEngine getEngine() {
        if(engine == null) {
            engine = AWREngine.getInstance(this);
            engine.setLoggingLevel(AnywareEngine.LOG_LEVEL_DEBUG);
            engine.setMaxDataUsage(1024);
            engine.setBluetoothActivationMode(AnywareEngine.BLUETOOTH_ACTIVATION_MODE_AUTOMATIC);
            Log.i(TAG, "Engine created from package "+getContext().getPackageName());
        }
        return engine;
    }

    public static void queueNotification(Intent notificationIntent) {
        notificationQueue.add(notificationIntent);
    }

    private void parseQueuedNotifications() {
        if(!notificationQueue.isEmpty()) {
            Log.v(TAG, "Parsing queued notifications");
            for(Intent notificationIntent : notificationQueue) {
                getEngine().parseNotification(notificationIntent);
            }
        }
    }

    private String dateToString(Date date) {
        return dateFormat.format(date);
    }

    private Date parseDate(String date) throws ParseException {
        date = date.replace("Z", "+0000");  //because the SimpleDateFormat cannot parse 'Z'
        return dateFormat.parse(date);
    }

    /**
     * Executes the request and returns PluginResult.
     * @param action        The action to execute.
     * @param args          JSONArray of arguments for the plugin.
     * @param command       The callback context used when calling back into JavaScript.
     * @return              The validity of the action.
     */
    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext command) throws JSONException {
        if(actions.contains(action)) {
            this.cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    if (action.equals("startEngine")) {
                        startEngine(args, command);
                    } else if (action.equals("stopEngine")) {
                        stopEngine(command);
                    } else if (action.equals("bindEngineWithUserId")) {
                        addCustomIdentifier(args, command);
                    } else if(action.equals("getDeviceId")) {
                        getDeviceId(command);
                    } else if(action.equals("getNotificationsSince")) {
                        getNotificationsSince(args, command);
                    } else if (action.equals("getNearestZone")) {
                        getNearestZone(command);
                    } else if (action.equals("findDevicesFromUserIds")) {
                        fetchDeviceLocations(args, command);
                    } else if (action.equals("setNotificationsEnabled")) {
                        setNotificationsEnabled(args, command);
                    } else if (action.equals("setupNotifications")) {
                        setupNotifications(args, command);
                    }
                }
            });
            return true;
        } else {
            Log.w(TAG, "Invalid action: "+action);
        }
        return false;
    }

    private void startEngine(JSONArray args, CallbackContext command) {
        parseQueuedNotifications();
        try {
            String apiKey = args.getString(0);
            Log.v(TAG, "Starting engine with API KEY: "+apiKey);
            getEngine().startEngine(apiKey, true);
            command.success();
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void stopEngine(CallbackContext command) {
        Log.v(TAG, "Stopping engine");
        getEngine().stopEngine();
        command.success();
    }

    private void addCustomIdentifier(JSONArray args, CallbackContext command) {
        try {
            String key = args.getString(0);
            String userId = args.getString(1);
            Log.v(TAG, String.format("Binding engine with custom identifier (%s, %s)", key, userId));
            getEngine().addCustomIdentifier(key, userId);
            command.success();
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void getDeviceId(CallbackContext command) {
        Log.v(TAG, "Getting device id");
        String deviceId = new Long(getEngine().getDeviceId()).toString();
        command.success(deviceId);
    }

    private void getNotificationsSince(JSONArray args, CallbackContext command) {
        try {
            String dateString = args.getString(0);
            Log.v(TAG, "Getting notifications since "+dateString);
            JSONArray notifications = getEngine().getNotificationsSince(parseDate(dateString));
            command.success(notifications);
        } catch (Exception ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void getNearestZone(CallbackContext command) {
        Log.v(TAG, "Getting nearest zone");
        AWRZone nearestZone = getEngine().getNearestZone();
        try {
            JSONObject data = new JSONObject();
            if(nearestZone != null) {
                data.put("name", nearestZone.getName());
                JSONObject metadata = new JSONObject(nearestZone.getMetaData());
                data.put("metadata", metadata);
            }
            command.success(data);
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void fetchDeviceLocations(JSONArray args, CallbackContext command) {
        try {
            String key = args.getString(0);
            ArrayList<String> userIds = new ArrayList<String>();
            JSONArray array = args.getJSONArray(1);
            for(int i=0; i<array.length(); i++) {
                userIds.add(array.getString(i));
            }
            Log.v(TAG, String.format("Finding devices from userIds (%s, %s)", key, array.toString()));
            getEngine().fetchDeviceLocations(key, userIds);
            command.success();
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void setNotificationsEnabled(JSONArray args, CallbackContext command) {
        try {
            boolean enabled = args.getBoolean(0);
            Log.v(TAG, "Setting notifications enabled: "+enabled);
            getEngine().setNotificationsEnabled(enabled);
            command.success();
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void setupNotifications(JSONArray args, CallbackContext command) {
        try {
            JSONObject options = args.getJSONObject(0);

            // Set notification activity
            options.put(AnywareEngine.NOTIFICATION_OPTION_ACTIVITY_CLASS_NAME, NotificationActivity.class.getName());
            // Set icon
            if (options.optString("icon_id").isEmpty()) {
                options.put(AnywareEngine.NOTIFICATION_OPTION_SMALL_ICON, getContext().getResources().getIdentifier("generic", "drawable", getContext().getPackageName()));
            } else {
                options.put(AnywareEngine.NOTIFICATION_OPTION_SMALL_ICON, getContext().getResources().getIdentifier(options.getString("icon_id"), "drawable", getContext().getPackageName()));
            }
            // Set background color
            if(!options.optString(AnywareEngine.NOTIFICATION_OPTION_COLOR).isEmpty()) {
                options.put(AnywareEngine.NOTIFICATION_OPTION_COLOR, Color.parseColor(options.getString(AnywareEngine.NOTIFICATION_OPTION_COLOR)));
            }

            Log.v(TAG, "Setuping notifications with options: "+options.toString());
            getEngine().setupNotifications(options);
            command.success();
        } catch (JSONException ex) {
            ex.printStackTrace();
            command.error(ex.getMessage());
        }
    }

    private void fireEvent(String event, JSONObject data) {
        final String js = String.format("anyware.notify('%s', %s);", event, data.toString());
        webView.post(new Runnable(){
            public void run(){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(js, null);
                } else {
                    webView.loadUrl("javascript:" + js);
                }
            }
        });
    }

    @Override
    public void engineDidStart() {
        Log.v(TAG, "engineDidStart");
        fireEvent("engine_start", new JSONObject());
    }

    @Override
    public void engineDidFailToStart(String error) {
        Log.v(TAG, "engineDidFailToStart: "+error);
        try {
            JSONObject data = new JSONObject();
            data.put("error", error);
            fireEvent("engine_start_fail", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didEnterZone(AWRZoneEvent zoneEvent) {
        Log.v(TAG, "didEnterZone: "+zoneEvent.toString());
        try {
            JSONObject data = new JSONObject();
            data.put("name", zoneEvent.getZone().getName());
            data.put("metadata", zoneEvent.getZone().getMetaData());
            fireEvent("zone_enter", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didExitZone(AWRZoneEvent zoneEvent) {
        Log.v(TAG, "didExitZone: "+zoneEvent.toString());
        try {
            JSONObject data = new JSONObject();
            data.put("name", zoneEvent.getZone().getName());
            data.put("metadata", zoneEvent.getZone().getMetaData());
            fireEvent("zone_exit", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didStayInZone(AWRZoneEvent zoneEvent) {
        Log.v(TAG, "didStayInZone: "+zoneEvent.toString());
        try {
            JSONObject data = new JSONObject();
            data.put("name", zoneEvent.getZone().getName());
            data.put("metadata", zoneEvent.getZone().getMetaData());
            fireEvent("zone_stay", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didReceivedGPSUpdate(AWRGPSDetection gpsDetection) {
        Log.v(TAG, "didReceivedGPSUpdate: "+gpsDetection.toString());
        //TODO maybe fire event "gps_update"
    }

    @Override
    public void didFailToConsumeNotification(Intent intent) {
        Log.v(TAG, "didFailToConsumeNotification");
        try {
            JSONObject data = new JSONObject();
            data.put("error", "No details about failure."); //TODO have an error message
            fireEvent("consume_notification_fail", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didConsumePlainTextNotification(Intent intent, String title, String text, Date date) {
        Log.v(TAG, "didConsumePlainTextNotification");
        try {
            JSONObject data = new JSONObject();
            data.put("title", title);
            data.put("message", text);
            data.put("date", dateToString(date));
            fireEvent("consume_plain_text_notification", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didConsumeWebPageNotification(Intent intent, String title, String text, Date date, String url) {
        Log.v(TAG, "didConsumeWebPageNotification");
        try {
            JSONObject data = new JSONObject();
            data.put("title", title);
            data.put("message", text);
            data.put("url", url);
            data.put("date", dateToString(date));
            fireEvent("consume_web_page_notification", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void didConsumeCustomActionNotification(Intent intent, String title, String text, Date date, HashMap parameters) {
        Log.v(TAG, "didConsumeCustomActionNotification");
        try {
            JSONObject data = new JSONObject();
            data.put("title", title);
            data.put("message", text);
            data.put("parameters", new JSONObject(parameters));
            data.put("date", dateToString(date));
            fireEvent("consume_custom_action_notification", data);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Context getContext() {
        return this.cordova.getActivity();
    }

    @Override
    public void didReceiveDeviceLocations(JSONObject devices) {
        Log.v(TAG, "didReceiveDeviceLocations: "+devices.toString());
        fireEvent("device_locations", devices);
    }
}
