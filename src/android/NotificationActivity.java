package com.novomnetworks.anyware.cordova;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class NotificationActivity extends Activity {
    public static final String TAG = "NotificationActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, "onCreate");

        try {
            // If this activity is the root activity of the task, the app is not running
            if (isTaskRoot()) {
                Log.v(TAG, "queueing notification");
                // Queue the notification to parse after the app is started
                AnywarePlugin.queueNotification(getIntent());
                // Start the app before finishing
                launchApp();
            } else {
                Log.v(TAG, "parsing notification");
                AnywarePlugin.getInstance().getEngine().parseNotification(getIntent());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finish();
    }

    /**
     * Launch main intent from package.
     */
    public void launchApp() {
        Context context = getApplicationContext();
        String pkgName = context.getPackageName();

        Intent intent = context.getPackageManager().getLaunchIntentForPackage(pkgName);

        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Log.v(TAG, "launching app");
        context.startActivity(intent);
    }
}